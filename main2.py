import sys
import copy
import ubqp as prob1
import voyageur_de_commerce2 as prob2
import numpy as np

# --fonction pour


def steepest_hill_climbing_inner_loop(Q, contrainte, solution_depart, calcul, comparaison, test_contrainte, meilleur_voisin):
    solution = solution_depart(Q, contrainte, test_contrainte)
    sp = solution
    MAX_DEPLACEMENT = 100
    nb_deplacement, stop = 0, False
    while nb_deplacement < MAX_DEPLACEMENT and not stop:
        autre_solution = meilleur_voisin(Q, sp, contrainte)
        if comparaison(Q, autre_solution, sp, calcul):
            sp = autre_solution
        else:
            stop = True
        nb_deplacement += 1

    return (nb_deplacement, solution, sp)


def steepest_hill_climbing_outer_loop(Q, contrainte, solution_depart, calcul, comparaison, test_contrainte, meilleur_voisin):
    MAX_ESSAI = 30
    nb_essai = 0
    solution_dep = solution_depart(Q, contrainte, test_contrainte)
    meilleur_solution_actuel = (0, solution_dep, solution_dep)

    while nb_essai < MAX_ESSAI:
        new_sol = steepest_hill_climbing_inner_loop(
            Q, contrainte, solution_depart,  calcul, comparaison, test_contrainte, meilleur_voisin)
        print(new_sol, calcul(Q, new_sol[2]))
        if comparaison(Q, new_sol[2], meilleur_solution_actuel[2], calcul):
            meilleur_solution_actuel = new_sol
            # print(meilleur_solution_actuel, calcul(
            #    Q, meilleur_solution_actuel[2]))
        nb_essai += 1

    return meilleur_solution_actuel, calcul(Q, meilleur_solution_actuel[2])


def non_tabou(solution, tabou, already_exist):
    return not already_exist(solution, tabou)


def voisin_non_tabou_prb1(solution, tabou, contrainte, test_contrainte, already_exist):
    voisin = []
    for i in range(solution.shape[0]):
        tmp = copy.copy(solution)
        tmp[i] = 1 if tmp[i] == 0 else 0
        if non_tabou(tmp, tabou, already_exist) and (test_contrainte(tmp, contrainte) if test_contrainte != None else True):
            voisin.append(copy.copy(tmp))
    return copy.copy(voisin)


def voisin_non_tabou_prb2(solution, tabou, contrainte, test_contrainte, already_exist):
    voisin = []
    for i in np.arange(0, Q.shape[0]-1):
        for j in np.arange(i+1, Q.shape[0]):
            copy_x = copy.copy(solution)
            tmp = copy_x[i]
            copy_x[i] = copy_x[j]
            copy_x[j] = tmp
            if non_tabou(copy_x, tabou, already_exist):
                voisin.append(copy.copy(copy_x))
    return copy.copy(voisin)


def meilleur_voisin_non_tabou(Q, solution, vnt, comparaison, calcul):
    best_sol = solution
    for possible_sol in vnt:
        if comparaison(Q, possible_sol, best_sol, calcul):
            best_sol = copy.copy(possible_sol)
    return best_sol


def recherche_tabou(Q, contrainte, solution_depart, calcul, comparaison, test_contrainte, already_exist, taille_tabou, voisin_non_tabou):
    # ---Init---
    MAX_DEPLACEMENT = 10
    s_init = solution_depart(Q, contrainte, test_contrainte)
    s = (0, s_init, s_init)
    tabou = []
    nb_deplacement = 0
    msol = s
    stop = False
    sp = msol
    # ---Loop---
    while nb_deplacement < MAX_DEPLACEMENT and not stop:
        vnt = voisin_non_tabou(s[2], tabou, contrainte,
                               test_contrainte, already_exist)

        if len(vnt) > 0:
            sp = (s[0], s[1], meilleur_voisin_non_tabou(
                Q, s[2], vnt, comparaison, calcul))
        else:
            stop = True

        sp = (sp[0]+1, sp[1], sp[2])

        if(len(tabou) < taille_tabou):
            tabou.append(s[2])
        if comparaison(Q, sp[2], msol[2], calcul):
            msol = sp

        s = sp
        nb_deplacement += 1

    return msol, calcul(Q, msol[2])


if __name__ == "__main__":
    # ---Main---
    if(len(sys.argv) < 4):
        print("J'ai besoin d'un fichier et num prob")
        exit(1)
    num_prob = int(sys.argv[2])
    num_met = int(sys.argv[3])
    f = open(sys.argv[1], "r")
    if num_prob == 1:
        p, Q = prob1.get_data(f)
        if num_met == 1:
            sol = steepest_hill_climbing_outer_loop(
                Q, p, prob1.solution_initial, prob1.calcul_solution, prob1.meilleur, prob1.contrainte_ok, prob1.meilleur_voisin)
            print(sol)
        else:
            sol_1 = recherche_tabou(Q, p, prob1.solution_initial,
                                    prob1.calcul_solution, prob1.meilleur, prob1.contrainte_ok, prob1.is_in, 3, voisin_non_tabou_prb1)
            print(sol_1)
    else:
        p, Q = prob2.get_data(f)
        if num_met == 1:
            #sol = prob2.calcul_solution(Q, prob2.solution_initial(Q, None, None))
            sol = steepest_hill_climbing_outer_loop(
                Q, p, prob2.solution_initial, prob2.calcul_solution, prob2.meilleur, None, prob2.meilleur_voisin
            )
            print(sol)
        else:
            sol_1 = recherche_tabou(Q, p, prob2.solution_initial,
                                    prob2.calcul_solution, prob2.meilleur, None, prob2.is_in, 40, voisin_non_tabou_prb2)
            print(sol_1)
