import numpy as np
import sys
import random
import collections
import copy


def get_data(file):
    data = [int(v) for v in file.readline().split(' ')]
    taille, p = data.pop(0), data.pop(0)
    m = np.matrix(data).reshape((taille, taille))
    return p, m


def contrainte_ok(solution, contrainte):
    return solution.sum() >= contrainte


def solution_initial(Q, contrainte, test_contrainte):
    p = np.random.randint(0, 2, Q.shape[1])
    while (test_contrainte(p, contrainte) if test_contrainte != None else False):
        p = np.random.randint(0, 2, Q.shape[1])
    return p


def calcul_solution(Q, X):
    solution = 0
    for i in range(Q.shape[0]):
        for j in range(Q.shape[1]):
            solution += Q.item((i, j))*X[i]*X[j]
    return solution


def meilleur_voisin(Q, X, p):
    valeur_solution_initial = calcul_solution(Q, X)
    ens_meilleur_voisin = []
    for i in range(X.shape[0]):
        tmp = copy.copy(X)
        tmp[i] = 1 if tmp[i] == 0 else 0
        sol = calcul_solution(Q, tmp)

        if(sol < valeur_solution_initial and tmp.sum() >= p):
            ens_meilleur_voisin.append(tmp)

    nb_meilleur_solution_trouver = len(ens_meilleur_voisin)
    if nb_meilleur_solution_trouver == 1:
        return ens_meilleur_voisin[0]
    elif nb_meilleur_solution_trouver > 1:
        return ens_meilleur_voisin[random.randint(0, nb_meilleur_solution_trouver-1)]
    else:
        return X


def meilleur(Q, solution_a, solution_b, calcul):
    return (calcul(Q, solution_a) < calcul(Q, solution_b))


def is_in(sol, tab):
    find = False
    for e in tab:
        same = True
        for i in range(len(sol)):
            if e[i] != sol[i]:
                same = False
        if same:
            find = True
    return find
