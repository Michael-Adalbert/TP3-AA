import numpy as np
import sys
import random
import collections
import copy
import math


def get_data(file):
    data = []
    taille = int(file.readline())
    for _ in range(taille):
        for v in file.readline().split('\t'):
            data.append(int(v))
    m = np.matrix(data).reshape((taille, 3))
    return None, m


def meilleur(Q, solution_a, solution_b, calcul):
    return (calcul(Q, solution_a) < calcul(Q, solution_b))


def dist_pt(a, b):
    res = math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)
    return res


def meilleur_voisin(Q, X, p):
    meilleur_voisin = X
    better_trouver = False
    sol_actuel = calcul_solution(Q, X)
    for i in np.arange(0, Q.shape[0]-1):
        for j in np.arange(i+1, Q.shape[0]):
            copy_x = copy.copy(X)
            tmp = copy_x[i]
            copy_x[i] = copy_x[j]
            copy_x[j] = tmp
            if calcul_solution(Q, copy_x) < sol_actuel:
                meilleur_voisin = copy_x
                better_trouver = True
                break
        if better_trouver:
            break

    return meilleur_voisin


def calcul_solution(Q, X):
    dep = 0.0
    origine = [0, 0]
    next_point = [0, 0]
    for v in X:
        n = Q[v-1]
        n.resize(3)
        next_point[0], next_point[1] = n[1], n[2]
        tmp = dist_pt(origine, next_point)
        origine = copy.copy(next_point)
        dep = dep + tmp
    dep += dist_pt(origine, [0, 0])
    return dep


def solution_initial(Q, contrainte, test_contrainte):
    s = np.array([i for i in Q[:, 0].flat])
    np.random.shuffle(s)
    return s


def is_in(sol, tab):
    find = False
    for e in tab:
        same = True
        for i in range(np.size(sol)):
            if e[i] != sol[i]:
                same = False
        if same:
            find = True
    return find
